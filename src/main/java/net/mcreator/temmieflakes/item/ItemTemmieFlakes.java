
package net.mcreator.temmieflakes.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemFood;
import net.minecraft.item.Item;
import net.minecraft.item.EnumAction;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.temmieflakes.creativetab.TabTEMMIEMOD;
import net.mcreator.temmieflakes.ElementsTemmieFlakes;

@ElementsTemmieFlakes.ModElement.Tag
public class ItemTemmieFlakes extends ElementsTemmieFlakes.ModElement {
	@GameRegistry.ObjectHolder("temmieflakes:temmieflakes")
	public static final Item block = null;
	public ItemTemmieFlakes(ElementsTemmieFlakes instance) {
		super(instance, 1);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemFoodCustom());
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(block, 0, new ModelResourceLocation("temmieflakes:temmieflakes", "inventory"));
	}
	public static class ItemFoodCustom extends ItemFood {
		public ItemFoodCustom() {
			super(4, 4f, false);
			setUnlocalizedName("temmieflakes");
			setRegistryName("temmieflakes");
			setAlwaysEdible();
			setCreativeTab(TabTEMMIEMOD.tab);
			setMaxStackSize(64);
		}

		@Override
		public EnumAction getItemUseAction(ItemStack par1ItemStack) {
			return EnumAction.EAT;
		}
	}
}
