
package net.mcreator.temmieflakes.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.Item;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.mcreator.temmieflakes.creativetab.TabTEMMIEMOD;
import net.mcreator.temmieflakes.ElementsTemmieFlakes;

@ElementsTemmieFlakes.ModElement.Tag
public class ItemTEMARMOR extends ElementsTemmieFlakes.ModElement {
	@GameRegistry.ObjectHolder("temmieflakes:temarmorhelmet")
	public static final Item helmet = null;
	@GameRegistry.ObjectHolder("temmieflakes:temarmorbody")
	public static final Item body = null;
	@GameRegistry.ObjectHolder("temmieflakes:temarmorlegs")
	public static final Item legs = null;
	@GameRegistry.ObjectHolder("temmieflakes:temarmorboots")
	public static final Item boots = null;
	public ItemTEMARMOR(ElementsTemmieFlakes instance) {
		super(instance, 7);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("TEMARMOR", "temmieflakes:em_armor", 25, new int[]{2, 5, 6, 2}, 100,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 5f);
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("temarmorhelmet")
				.setRegistryName("temarmorhelmet").setCreativeTab(TabTEMMIEMOD.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("temarmorbody").setRegistryName("temarmorbody")
				.setCreativeTab(TabTEMMIEMOD.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS).setUnlocalizedName("temarmorlegs").setRegistryName("temarmorlegs")
				.setCreativeTab(TabTEMMIEMOD.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET).setUnlocalizedName("temarmorboots")
				.setRegistryName("temarmorboots").setCreativeTab(TabTEMMIEMOD.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("temmieflakes:temarmorhelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("temmieflakes:temarmorbody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("temmieflakes:temarmorlegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("temmieflakes:temarmorboots", "inventory"));
	}
}
