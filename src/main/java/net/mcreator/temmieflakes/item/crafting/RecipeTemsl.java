
package net.mcreator.temmieflakes.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.mcreator.temmieflakes.item.ItemTemingot;
import net.mcreator.temmieflakes.block.BlockTemore;
import net.mcreator.temmieflakes.ElementsTemmieFlakes;

@ElementsTemmieFlakes.ModElement.Tag
public class RecipeTemsl extends ElementsTemmieFlakes.ModElement {
	public RecipeTemsl(ElementsTemmieFlakes instance) {
		super(instance, 13);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockTemore.block, (int) (1)), new ItemStack(ItemTemingot.block, (int) (1)), 10F);
	}
}
